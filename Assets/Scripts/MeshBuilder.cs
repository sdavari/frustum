﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class MeshBuilder : MonoBehaviour {
	private Mesh mesh;
	private Vector3[] vertices;
	private int[] triangles;
	private Vector2[] uvs;

	public GameObject gc;

	public Vector3 vertLeftTopFront = new Vector3 (-1, 1, 1);
	public Vector3 vertLeftBottomFront = new Vector3 (-1, -1, 1);
	public Vector3 vertRightTopFront = new Vector3 (1, 1, 1);
	public Vector3 vertRightBottomFront = new Vector3 (1, -1, 1);

	public Vector3 vertLeftTopBack = new Vector3 (-1, 1, -1);
	public Vector3 vertRightTopBack = new Vector3 (1, 1, -1);
	public Vector3 vertLeftBottomBack = new Vector3 (-1, -1, -1);
	public Vector3 vertRightBottomBack = new Vector3 (1, -1, -1);

	void Start () {
		mesh = GetComponent<MeshFilter> ().mesh;

		//Triangles (clockwise)
		triangles = new int[]{
			//front face
			//triangle 1
			0, 2, 3,
			//triangle 2
			3, 1, 0,

			//back face
			//triangle 1
			4, 6, 7,
			//triangle 2
			7, 5, 4,

			//left face
			//triangle 1
			8, 10, 11,
			//triangle 2
			11, 9, 8,

			//right face
			//triangle 1
			12, 14, 15,
			//triangle 2
			15, 13, 12,

			//top face
			//triangle 1
			16, 18, 19,
			//triangle 2
			19, 17, 16,

			//bottom face
			//triangle 1
			20, 22, 23,
			//triangle 2
			23, 21, 20
		};

		//UVs
		uvs = new Vector2[] {
			//front face
			// bottom left 0, 0
			// top right 1, 1
			new Vector2(0 ,1),
			new Vector2(0 ,0),
			new Vector2(1 ,1),
			new Vector2(1 ,0), 

			//front face
			// bottom left 0, 0
			// top right 1, 1
			new Vector2(0 ,1),
			new Vector2(0 ,0),
			new Vector2(1 ,1),
			new Vector2(1 ,0),

			//front face
			// bottom left 0, 0
			// top right 1, 1
			new Vector2(0 ,1),
			new Vector2(0 ,0),
			new Vector2(1 ,1),
			new Vector2(1 ,0),

			//front face
			// bottom left 0, 0
			// top right 1, 1
			new Vector2(0 ,1),
			new Vector2(0 ,0),
			new Vector2(1 ,1),
			new Vector2(1 ,0),

			//front face
			// bottom left 0, 0
			// top right 1, 1
			new Vector2(0 ,1),
			new Vector2(0 ,0),
			new Vector2(1 ,1),
			new Vector2(1 ,0),

			//front face
			// bottom left 0, 0
			// top right 1, 1
			new Vector2(0 ,1),
			new Vector2(0 ,0),
			new Vector2(1 ,1),
			new Vector2(1 ,0)
		};

	}
	
	void Update () {

		Matrix4x4 m = Camera.main.cameraToWorldMatrix;
		Vector3 eyeCenter = m.MultiplyPoint(Quaternion.Inverse(InputTracking.GetLocalRotation(XRNode.CenterEye)) * InputTracking.GetLocalPosition(XRNode.CenterEye));
		//Vector3 eyeCenter = Camera.main.transform.position;


		//Between Eyes
		vertLeftBottomBack = vertRightTopBack = vertLeftTopBack = vertRightBottomBack = eyeCenter;

		// gc corners
		SpriteRenderer sr = gc.GetComponent<SpriteRenderer>();
		Vector3 leftBottomFront = sr.bounds.min;
		Vector3 rightTopFront = sr.bounds.max;
		Vector3 rightBottomFront = new Vector3 (sr.bounds.max.x, sr.bounds.min.y, sr.bounds.max.z);
		Vector3 leftTopFront = new Vector3 (sr.bounds.min.x, sr.bounds.max.y, sr.bounds.max.z);


		//back face
		vertLeftBottomFront = eyeCenter + 15 * (leftBottomFront - eyeCenter);
		vertRightTopFront = eyeCenter + 15 * (rightTopFront - eyeCenter);
		vertRightBottomFront = eyeCenter + 15 * (rightBottomFront - eyeCenter);
		vertLeftTopFront = eyeCenter + 15 * (leftTopFront - eyeCenter);


		vertices = new Vector3[]{
			//front face
			vertLeftTopFront, //0 [left x=-1, top y=1, front z=1]
			vertRightTopFront, //1 [right x=1, top y=1, front z=1]
			vertLeftBottomFront, //2 [left x=-1, bottom y=-1, front z=1]
			vertRightBottomFront, //3 [right x=1, bottom y=-1, front z=1]

			//back face
			vertRightTopBack, //4 [right x=1, top y=1, back z=-1]
			vertLeftTopBack, //5 [left x=-1, top y=1, back z=-1]
			vertRightBottomBack, //6 [right x=1, bottom y=-1, back z=-1]
			vertLeftBottomBack, //7 [left x=-1, bottom y=-1, back z=-1]

			//left face
			vertLeftTopBack, //8 [left x=-1, top y=1, back z=-1]
			vertLeftTopFront, //9 [left x=-1, top y=1, frontz=1]
			vertLeftBottomBack, //10 [left x=-1, bottom y=-1, back z=-1]
			vertLeftBottomFront, //11 [left x=-1, bottom y=-1, front z=1]

			//right face
			vertRightTopFront, //12 [right x=1, top y=1, front z=1]
			vertRightTopBack, //13 [right x=1, top y=1, back z=-1]
			vertRightBottomFront, //14 [right x=1, bottom y=-1, front z=1]
			vertRightBottomBack, //15 [right x=1, bottom y=-1, back z=-1]

			//top face
			vertLeftTopBack, //16 [left x=-1, top y=1, back z=-1]
			vertRightTopBack, //17 [right x=1, top y=1, back z=-1]
			vertLeftTopFront, //18 [left x=-1, top y=1, front z=1]
			vertRightTopFront, //19 [right x=1, top y=1, front z=1]

			//bottom face
			vertLeftBottomFront, //20 [left x=-1, bottom y=-1, front z=1]
			vertRightBottomFront, //21 [right x=1, bottom y=-1, front z=1]
			vertLeftBottomBack, //22 [left x=-1, bottom y=-1, back z=-1]
			vertRightBottomBack //23 [right x=1, bottom y=-1, back z=-1]
			};
		mesh.Clear();
		mesh.vertices = vertices;
		mesh.triangles = triangles;
		mesh.uv = uvs;
		mesh.RecalculateNormals();
		mesh.RecalculateBounds();
		mesh.Optimize();	
	}
}
